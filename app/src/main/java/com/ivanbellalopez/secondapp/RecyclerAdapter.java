package com.ivanbellalopez.filereader;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList items;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            textView = (TextView) v.findViewById(R.id.textView);
        }
    }

    public RecyclerAdapter( Context context, ArrayList array) {

        this.context = context;
        this.items = array;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String itemId = String.valueOf(items.get(i));

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.nicequest.com")
                .appendPath("portal_nicequest_api")
                .appendPath("DocumentServlet")
                .appendQueryParameter("docid", itemId);
        String url = builder.build().toString();

        viewHolder.textView.setText(itemId);
        Picasso.with(context).load(url).into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

